package operation;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import controller.Operation;

public class RetirerTest {
	
	@Test
	public void retirer_nullNomClient_shouldLogErrorNonExistingClient() {
		//given
		Operation operation = new Operation();
		String nomClient = null;
		double montant = 500;
		
		//operation 
		String result = operation.retirer(nomClient, montant);
		
		//assert
		assertEquals("L'utilisateur n'existe pas", result);
	}
		
}
